package com.rave.practiceround3

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Hilt application class.
 *
 * @constructor Create empty Meal application
 */
@HiltAndroidApp
class MealApplication : Application()
