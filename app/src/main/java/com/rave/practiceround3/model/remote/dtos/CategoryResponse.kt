package com.rave.practiceround3.model.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
data class CategoryResponse(
    val categories: List<CategoryDTO>
)
