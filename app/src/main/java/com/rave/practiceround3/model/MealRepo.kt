package com.rave.practiceround3.model

import com.rave.practiceround3.model.local.Category
import com.rave.practiceround3.model.remote.APIService
import javax.inject.Inject

/**
 * Repository class to mediate how meal data is fetched.
 *
 * @property service
 * @constructor Create empty Meal repo
 */
class MealRepo @Inject constructor(private val service: APIService) {

    /**
     * Fetch Meal categories.
     *
     * @return
     */
    suspend fun getMealCategories(): List<Category> {
        val categoryDTOs = service.getMealCategories().categories
        return categoryDTOs.map {
            Category(
                idCategory = it.idCategory,
                strCategory = it.strCategory,
                strCategoryThumb = it.strCategoryThumb,
                strCategoryDescription = it.strCategoryDescription
            )
        }
    }
}
